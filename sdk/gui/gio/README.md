# gioui.org

https://git.sr.ht/~eliasnaur/gio/tree/master

Good Mirror: https://github.com/theclapp/gio-mirror

Issue: https://todo.sr.ht/~eliasnaur/gio


all his repos: https://git.sr.ht/~eliasnaur/

install: https://gioui.org/doc/install

https://git.sr.ht/~eliasnaur/scatter
- signal protocol: github.com/eliasnaur/libsignal-protocol-go
	- latest is: https://github.com/crossle/libsignal-protocol-go

Kitchen with new Layout :
https://git.sr.ht/~eliasnaur/giox/tree/master/cmd/kitchen/kitchen.go

Why ?
- pure golang so no split brain between flutter and golang ( and wasm)
- CRDT and Genji can be embeddded EASILY
- Desktop Webtc using Ion will work i think. Solves a bitch of a problem
- Packing MUCH easier.


## Search

https://github.com/search?l=Go&q=gioui.org%2Fapp&type=Code



## Test Harness

What we need is a test Harness that proves the various aspects !!

obfuscate
- https://github.com/mvdan/garble

ion needs audio and video
- Just a matter of hooking into your own code it seems: https://todo.sr.ht/~eliasnaur/gio/125

i18n
- left to right ? Not yet but van live with that.
- https://github.com/nicksnyder/go-i18n/tree/v2.0.3
	- most people use this and should be easy to use with our google translate and json tool.

packaging ?
- android: https://github.com/tailscale/tailscale-android/blob/master/Makefile

Cut and paste
- Seems it does work on all platforms: https://todo.sr.ht/~eliasnaur/gio/31

## Printing

- we only want to print PDF; because that is what we will output from the views.
- for mobile check what the flutter printing plugin calls.
- for desktop, we need our own Print preview, where we rasterise the PDF to an image and show it, and allow the to change the paper size, and then pump the pump to the printer.
- for web we need to just make a PDF and then hook into the Web preview and printing. 

Decision: Lets just call the OS level Print Dialogues !!
Pob need to make syscalls

https://github.com/sqweek/dialog
- Just OS dialogues for Message, File open, etc
https://github.com/gen2brain/dlgs
- dlgs is a cross-platform library for displaying dialogs and input boxes.
- also has js for gopherjs
https://github.com/gen2brain/beeep
- beeep provides a cross-platform library for sending desktop notifications, alerts and beeps
- up to date !!

macOS Print dialog
Print Dialog's 'Open PDF in Preview'
cupsfilter <your-image> | open -f -a "Preview"


Windows Print dialog

Linux Print dialog


- Google cloud print is dead. 
	- IPP is the new one.
	- http://pwg.org/
	- https://pwg.org/pwg-books/ippguide.html#what-is-ipp
	- port 631. scheme: ipps or ipp.
	- impl:
		- https://github.com/phin1x/go-ipp
		- https://www.papercut.com/kb/Main/InstallingIPPSPrinters
			- PDF search using bleve: https://github.com/PaperCutSoftware/pdfsearch
		- https://github.com/zmap/zgrab2/tree/master/modules/ipp
			- ipp scanner 


## Service daemon

Will need a proper desktop service so that system can run without GUI being open.
The idea is that peoples desktops can be their servers.

Desktop: https://github.com/PaperCutSoftware/silver/blob/master/service/main.go
- production tested


## Notifications
- Mobile
- Desktop. There are a few golang libs
- Web. New standards for this to work, so hunt around

Multi windows
- Yes for desktop: https://todo.sr.ht/~eliasnaur/gio/19
	- def needed too for video conf as you want to open a new window so you can still work in your main window.
	- see eliasnaur-gio/makefile. It works well !!! :)

WASM
- tiny go: Not yt it seems: https://todo.sr.ht/~eliasnaur/gio/98
- I dont ind having large golang compiled WASM for now, because its an APP, not a web page. And tinygo is still rough and will get better since its offically go team supported.

canvas
https://github.com/glycerine/hello_gio


https://github.com/marcetin
- lots of projects using it !
- has some wrappers to !

https://github.com/p9c
https://github.com/p9c/fui
- amzing stuff
- this gux dong it: https://github.com/l0k18
- pod looks like a good repo that brings it together: https://github.com/p9c/pod
- 

DB
https://github.com/genjidb/genji
- will be EASY to integrate

CRDT
https://github.com/yorkie-team/yorkie
- also easy to integrate

KAPPA on top so that CRDT and DB is invisible you can have the domain mutations be consistent
and you can store materialsed view in the DB and have them update based on mutations.
so its not just for documents but for any db like needs, and because DB has SQL you can do full search on your Materialised views also.




server
https://github.com/slackhq/nebula
- Nebula is a mutually authenticated peer-to-peer software defined network based on the Noise Protocol Framework.
- Lihthouse is the rendevous
- make looks good: https://github.com/slackhq/nebula/blob/master/Makefile




LIB_FSPATH=$(PWD)

print:
	@echo
	@echo LIB_FSPATH: 	$(LIB_FSPATH)
	@echo

dep:
	go get github.com/shurcooL/goexec
	go get gioui.org/cmd/gogio


desktop-run:
	# works fine
	cd $(LIB_FSPATH)/example && go run .

web-run:
	# or Desktop, works fine.
	# For Mobile web, the buttons dont work. This is a problme inherent to GIO
	cd $(LIB_FSPATH)/example && gogio -target js $(LIB)
	cd $(LIB_FSPATH)/example && goexec 'http.ListenAndServe(":8080", http.FileServer(http.Dir("materials")))'
	
ngrok-run:
	# For Testing from real mobile jsut in case i am crazy :)
	# joe account
	#ngrok authtoken 1exjsjk0TYQPCErukaaMMRMPSEi_vJvzPNCAQARgY4gBdTeE
	ngrok http 8080

LIB_FSPATH=$(PWD)
GIO_BIN_NAME=gogio
print:
	@echo
	@echo LIB_FSPATH: 	$(LIB_FSPATH)
	@echo

dep:
	go get github.com/shurcooL/goexec

### Step 1 - Build gogio

build:
	# build to go bin
	cd $(LIB_FSPATH)/cmd/gogio && go build -o $(GOPATH)/bin/$(GIO_BIN_NAME) .

	# check is global
	which $(GIO_BIN_NAME)


### Step 2 - Play with EXAMPLES

all-clean: build
	rm -rf  $(LIB_FSPATH)/bin
	rm -rf  $(LIB_FSPATH)/bin-www

	mkdir -p $(LIB_FSPATH)/bin
	mkdir -p $(LIB_FSPATH)/bin-www
kitchen-build: all-clean
	
	

	

	#www (tinygo)
	# not working. compiler entry point is all wrong
	#cd $(LIB_FSPATH)/example/kitchen && tinygo build -o $(PWD)/bin/kitchen/www -target wasm -no-debug kitchen.go

kitchen-desk-run:
	# desk
	cd $(LIB_FSPATH)/example/kitchen && go build -o $(LIB_FSPATH)/bin/kitchen .

	$(LIB_FSPATH)/bin/kitchen

kitchen-web-run: 
	# www
	cd $(LIB_FSPATH)/example/kitchen && $(GIO_BIN_NAME) -target js gioui.org/example/kitchen .
	mv $(LIB_FSPATH)/example/kitchen/kitchen $(LIB_FSPATH)/bin-www/kitchen
	goexec 'http.ListenAndServe(":8080", http.FileServer(http.Dir("$(LIB_FSPATH)/bin-www/kitchen")))'

kitchen-ios-run: 
	# ios
	cd $(LIB_FSPATH)/example/kitchen && $(GIO_BIN_NAME) -target ios -o kitchen.app gioui.org/example/kitchen .
	xcrun simctl install booted $(LIB_FSPATH)/example/kitchen/kitchen.app

kitchen-and-run: 
	# ios
	cd $(LIB_FSPATH)/example/kitchen && $(GIO_BIN_NAME) -target android -o kitchen.apk gioui.org/example/kitchen .
	adb install $(LIB_FSPATH)/example/kitchen/kitchen.app

	
	
kitchen-web-run-stats:
	# Found from: https://github.com/golang/go/issues/32591
	#cd $(LIB_FSPATH)/example/kitchen && $(GIO_BIN_NAME) -target js gioui.org/example/kitchen -stats
	cd $(LIB_FSPATH)/example/kitchen && $(GIO_BIN_NAME) -target js gioui.org/example/kitchen
	goexec -quiet 'http.ListenAndServe(":8080", http.FileServer(http.Dir("$(LIB_FSPATH)/example/kitchen/kitchen")))'




gophers-build: all-clean
	# desk
	cd $(LIB_FSPATH)/example/gophers && go build -o $(LIB_FSPATH)/bin/gophers

	# web
	cd $(LIB_FSPATH)/example/gophers && $(GIO_BIN_NAME) -target js gioui.org/example/gophers 
	mv $(LIB_FSPATH)/example/gophers/gophers $(LIB_FSPATH)/bin-www/gophers
	
gophers-desk-run: gophers-build
	$(LIB_FSPATH)/bin/gophers

gophers-web-run: gophers-build
	goexec -quiet 'http.ListenAndServe(":8080", http.FileServer(http.Dir("$(LIB_FSPATH)/bin-www/gophers")))'
	


tabs-build: all-clean
	# desk
	cd $(LIB_FSPATH)/example/tabs && go build -o $(LIB_FSPATH)/bin/tabs

	# web
	cd $(LIB_FSPATH)/example/tabs && $(GIO_BIN_NAME) -target js gioui.org/example/tabs . 
	mv $(LIB_FSPATH)/example/tabs/tabs $(LIB_FSPATH)/bin-www/tabs
	
tabs-desk-run: tabs-build
	$(LIB_FSPATH)/bin/tabs

tabs-web-run: tabs-build
	#cd $(LIB_FSPATH)/bin-www && goexec 'http.ListenAndServe(":8080", http.FileServer(http.Dir("tabs")))'
	goexec -quiet 'http.ListenAndServe(":8080", http.FileServer(http.Dir("$(LIB_FSPATH)/bin-www/tabs")))'




windows-build: all-clean
	# desk
	cd $(LIB_FSPATH)/example/windows && go build -o $(LIB_FSPATH)/bin/windows

	# web
	cd $(LIB_FSPATH)/example/windows && $(GIO_BIN_NAME) -target js gioui.org/example/windows . 
	mv $(LIB_FSPATH)/example/windows/windows $(LIB_FSPATH)/bin-www/windows

windows-desk-run: windows-build
	$(LIB_FSPATH)/bin/windows

windows-web-run: windows-build
	goexec -quiet 'http.ListenAndServe(":8080", http.FileServer(http.Dir("$(LIB_FSPATH)/bin-www/windows")))'


ngrok-run:
	# For Testing from real mobile jsut in case i am crazy :)
	# joe account
	#ngrok authtoken 1exjsjk0TYQPCErukaaMMRMPSEi_vJvzPNCAQARgY4gBdTeE
	ngrok http 8080
LIB_FSPATH=$(PWD)
GIO_BIN_NAME=gogio

print:
	@echo
	@echo LIB_FSPATH: 	$(LIB_FSPATH)
	@echo

dep:
	go get github.com/markbates/pkger/cmd/pkger



clean: build
	rm -rf  $(LIB_FSPATH)/bin
	rm -rf  $(LIB_FSPATH)/bin-www

	mkdir -p $(LIB_FSPATH)/bin
	mkdir -p $(LIB_FSPATH)/bin-www

build: clean

	#www (tinygo)
	# not working. compiler entry point is all wrong
	#cd $(LIB_FSPATH)/example/kitchen && tinygo build -o $(PWD)/bin/kitchen/www -target wasm -no-debug kitchen.go

desk-run: build
	# desk
	cd $(LIB_FSPATH) && go build -o $(LIB_FSPATH)/bin/godcr .

	cd $(LIB_FSPATH) && ./bin/godcr

web-run: 
	# www
	cd $(LIB_FSPATH) && $(GIO_BIN_NAME) -target js github.com/raedahgroup/godcr .
	mv $(LIB_FSPATH)/godcr/ $(LIB_FSPATH)/bin-www/godcr
	goexec 'http.ListenAndServe(":8080", http.FileServer(http.Dir("$(LIB_FSPATH)/bin-www/kitchen")))'

ios-run: 
	# ios
	cd $(LIB_FSPATH) && $(GIO_BIN_NAME) -target ios -o godcr.app github.com/raedahgroup/godcr .
	xcrun simctl install booted $(LIB_FSPATH)/godcr.app

and-run: 
	# ios
	cd $(LIB_FSPATH)/example/kitchen && $(GIO_BIN_NAME) -target android -o kitchen.apk gioui.org/example/kitchen .
	adb install $(LIB_FSPATH)/example/kitchen/kitchen.app

	
ngrok-run:
	# For Testing from real mobile jsut in case i am crazy :)
	# joe account
	#ngrok authtoken 1exjsjk0TYQPCErukaaMMRMPSEi_vJvzPNCAQARgY4gBdTeE
	ngrok http 8080
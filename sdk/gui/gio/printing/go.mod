module codeberg.org/gerard.webb/junk/sdk-gui/gio/printing

go 1.14

require (
	github.com/alexbrainman/printer v0.0.0-20181008173622-345afe414dee // indirect
	github.com/jadefox10200/goprint v0.0.0-20180330192300-fba75953845c // indirect
	github.com/jung-kurt/gofpdf v1.16.2 // indirect
)

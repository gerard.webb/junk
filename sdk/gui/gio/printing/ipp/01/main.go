package main

import (
	"github.com/phin1x/go-ipp"
)

func main() {

	// create a new ipp client
	//client := ipp.NewIPPClient("printserver", 631, "user", "password", true)

	client := ipp.NewIPPClient("localhost", 631, "", "", true)

	// print file
	client.PrintFile("test.pdf", "my-printer", map[string]interface{}{})
}

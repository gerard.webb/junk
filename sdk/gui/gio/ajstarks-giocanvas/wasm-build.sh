#!/bin/bash


# build index.html
mkdir -p www
cd www && touch index.html
echo $'<h1>GIO Canvas Test Harness</h1>' > index.html
cd ..

# build nav links to each demo
for i in $(cat cl)
do
	echo "<a href=\"/$i/www/index.html\">$i</a>" >> www/index.html
done


# build each demo
for i in $(cat cl)
do
	cd $i
	gogio -target js github.com/ajstarks/giocanvas .

	# copy up into www folder
	mkdir ../www/$i
	mv giocanvas ../www/$i/www
	
	cd ..
done
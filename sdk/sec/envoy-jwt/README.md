# envoy jwt

Lets just use simple JWT

 "@type": type.googleapis.com/envoy.extensions.filters.http.jwt_authn.v3.JwtAuthentication

https://www.envoyproxy.io/docs/envoy/latest/api-v2/config/filter/http/jwt_authn/v2alpha/config.proto

https://www.envoyproxy.io/docs/envoy/latest/start/sandboxes/front_proxy.html
https://www.envoyproxy.io/learn/front-proxy

- JWT means the token is sent by the Client on each request, and so the server is staleless and so we cna scale to a global load balanced setup.
	- "CMD /usr/local/bin/envoy -c /etc/front-envoy.yaml --service-cluster front-proxy"

AuthZ example:
https://github.com/envoyproxy/envoy/tree/master/examples/ext_authz

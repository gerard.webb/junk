# Firebase

What if we offload all the auth and stuff to firebase

Our golang server can be part of the flow.
https://github.com/firebase/firebase-admin-go
We can have the Orgs code call out stateless Gateway Server and it will send the message to the Firebase system.
That allows use to control it all.
The same goes for Messages from the app. It goes to our gateway and we just forward it to the Org Server.
Because all Users are running the same APP, all messages will go to our Server, so it will all work.



The flutter tooling does all the rest for use
- Only supports web and mobile and Mac. No Windows yet.
https://pub.dev/packages/firebase_auth

Auth
- https://github.com/FirebaseExtended/flutterfire/tree/master/packages/firebase_auth

We can also use the other bits still and call them from go
Analytics
- https://github.com/FirebaseExtended/flutterfire/tree/master/packages/firebase_analytics
Crash
- https://github.com/FirebaseExtended/flutterfire/tree/master/packages/firebase_crashlytics
Notifications
- https://pub.dev/packages/firebase_messaging
- https://github.com/FirebaseExtended/flutterfire/tree/master/packages/firebase_messaging
App Distribution
https://blog.codemagic.io/deploying-flutter-app-to-firebase-app-distribution-using-fastlane/





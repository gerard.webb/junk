# security

We need the usuals Identity stuff for Servers and Clients
Auth
https://github.com/ory/kratos#what-is-ory-kratos
- login, etc

https://github.com/ory/fosite
- lighter and with example: github.com/ory/fosite-example


Authz
https://github.com/ory/keto#introduction
- role based access control


https://github.com/ory/ladon
- lighter with mem or CR DB


----

NATS based
https://bitbucket.org/cstep/stan.dart/src
- Looks like working Flutter client, but not sure if it works as a client
- Uses: https://bitbucket.org/cstep/nats.dart/src/develop/pubspec.yaml



GRPC based with Flutter

code: https://github.com/grpc/grpc-dart
pub: https://pub.dev/packages/grpc

Remote UI FLutter
https://github.com/softmarshmallow/remote-ui
- self describes the GUI from the backend using grpc.

Threads
threads_client : https://github.com/textileio/dart-threads-client/blob/master/pubspec.yaml#L11
uses threads_client_grpc: https://github.com/textileio/go-threads/blob/master/api/pb/dart/pubspec.yaml




https://github.com/Tobi696/blog-application/blob/master/backend/AuthService/service.go
- grpc
- mongodb
- GUi is vue, not flutter 

---

doc: https://steemit.com/utopian-io/@tensor/working-with-grpc-in-flutter-on-top-of-a-go-service
flu: https://github.com/tensor-programming/flutter-grpc-client-example
go: https://github.com/tensor-programming/docker_grpc_chat_tutorial/blob/master/main.go


https://github.com/256dpi/fire
- mongoDB with RPC and WS and SEE
- has Oauth and auth and authz to the mongo collections and gridfs
- lungo is the store
- https://github.com/256dpi/turing is a pebble based DB that can work wth clusters or embeded
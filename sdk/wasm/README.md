# wasm

WASM, WASI, WASMR

## Video COnf

We can use Websockets and WASM for a Video COnf system and avoid ALL the insane shit with do with Webrtc.
This is exactly how zoom does it: https://webrtchacks.com/zoom-avoids-using-webrtc/
See also this: https://www.w3.org/2011/04/webrtc/wiki/images/5/5c/WebRTCWG-2018-06-19.pdf
- They are proposing exactly what i am. WASM in Webworkers !
- Allows QUIC to be used and FULL control 
QUIC has no head of line blocking and so will be even better than web sockets, but QUIC is NOT yet in browser.
Video and audio access is easy.

THE key question is "On Desktop of Mobile", when the flutter web app is "Saves to the home screen", do we have access to the Media API ?
- If we do then we are good !! 

## GUI

Vugo Now comples using tinygo with WASM !!
See updates at: https://www.vugu.org/


## Registry

Need a place for users to register their code.

Registry for wasm.
Golang
https://radu-matei.com/blog/wasm-to-oci/

## Runners

K8 wasm runner
Krustlet

https://cloudblogs.microsoft.com/opensource/2020/04/07/announcing-krustlet-kubernetes-rust-kubelet-webassembly-wasm/

Desktop 
Runtime for wasm on desktop
https://github.com/bytecodealliance/wasmtime-go
Golang
Can I run this on mobile ??

IOT
https://github.com/bytecodealliance/wasm-micro-runtime


## Compilers

Wasm compiler for go code
Tinygo
Wasmer

https://wasmer.io/
About 10 languages: https://github.com/wasmerio/wasmer#language-integrations
Golang: https://github.com/wasmerio/go-ext-wasm


List of Lang's that compile to wasm: 
https://github.com/appcypher/awesome-wasm-langs/blob/master/README.md
- tinygo would be my favourite


## Flutter wasm
https://pub.dev/packages/flutter_wasm_interop
https://github.com/rodydavis/flutter_wasm_interop
https://github.com/rodydavis/flutter_wasm_example

example: https://github.com/rodydavis/flutter_wasm_example

Can use web, desktop and mobile
- uses a wasm runtime for all native. Wasmer via FFI
- looks like flutter team want to support wasm. It's also freaking smart because then once you get it working all code you deploy does not have to be cross compiled : https://github.com/dart-lang/sdk/issues/37882
- someone has git running as wasm inside flutter : https://github.com/petersalomonsen/wasm-git/blob/master/README.md




## FLutter ffi

From flutter we need to call native

I really want to use protobufs for web, native and go-flutter

https://hacks.mozilla.org/2019/04/crossing-the-rust-ffi-frontier-with-protocol-buffers/
- explains it and why 
- some or ALL of the code we want to use is rust, and for protobuf they tend to use: https://github.com/danburkert/prost

--

dart FFI plugin users: https://pub.dev/packages?q=dependency%3Affi&sort=updated


---

https://gitlab.com/vocdoni/dvote-flutter-native/-/blob/master/lib/dvote_native.dart
- calls rust
- flutter code calling it: https://gitlab.com/vocdoni/dvote-flutter/-/blob/master/pubspec.yaml
- flutter wbe with wasm , not working yet: https://gitlab.com/vocdoni/dvote-wasm/-/tree/master

https://github.com/jerson/flutter-openpgp
- Has flutter go desktop, mobile and web :)
- native golang code here: https://github.com/jerson/openpgp-mobile


embly
https://github.com/embly/embly
- Server based running WASM.
- good first step actually, because then the Modules can run off the Server, but later on the client
- Its actually a open source version of cloudflare workers ( https://workers.cloudflare.com/ ) )

---

## Bootstrap Servers

- Cloudflare workers with CRDT WAL between them looks ideal. Light and simple.
- must cross compile to JS ? 




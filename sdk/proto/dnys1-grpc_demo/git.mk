
# https://github.com/dnys1/grpc_demo

# For dev
# Ports. See:https://github.com/dnys1/grpc_demo/blob/master/client/lib/core/config/config.dart
# If web local, flutter calls Envoy at 8080, which upstreads to golang at 50051
# If web real, flutter calls Envoy at 8080, which upstreads to golang at 50051

# if mobile simulator, flutter calls 127.0.0.1 on port 19028
# If mobile device, flutter calls 2.tcp.ngrok.io on port 50051 ( golang server )

# SO Native client bypass envoy totally
# So do all auth etc at the golang level for everything !! Makes it way easier 




include ./../../root.mk


LIB_REPO=				github.com/dnys1
LIB_REPO_FSPATH=		$(GOPATH)/src/$(LIB_REPO)


this-print:
	@echo LIB_REPO: 			$(LIB_REPO)
	@echo LIB_REPO_FSPATH: 		$(LIB_REPO_FSPATH)

this-git-clone:
	$(MAKE) git-clone-args LIB=$(LIB_REPO)/grpc_demo LIB_FSPATH=$(LIB_REPO_FSPATH)/grpc_demo


this-git-delete:
	rm -rf $(LIB_REPO_FSPATH)



this-vscode-add:
	# copy vscode debug json into project
	mkdir -p $(LIB_REPO_FSPATH)/.vscode
	cp ./launch.json $(LIB_REPO_FSPATH)/.vscode/.

	# copy make file in
	cp ./makefile $(LIB_REPO_FSPATH)/.

	# Now add the project as a workspace
	code --add $(LIB_REPO_FSPATH)


this-vscode-open:

	# open vscode
	code $(LIB_REPO_FSPATH)
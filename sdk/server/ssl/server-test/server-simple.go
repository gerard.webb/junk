package main

import (
	// "fmt"
	// "io"
	"log"
	"net/http"
)

// HelloServer aeah
func HelloServer(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("This is an example server.\n"))
	// fmt.Fprintf(w, "This is an example server.\n")
	// io.WriteString(w, "This is an example server.\n")
}

func main() {
	http.HandleFunc("/hello", HelloServer)
	err := http.ListenAndServeTLS(":8443", "_wildcard.localhost.pomerium.io.pem", "_wildcard.localhost.pomerium.io-key.pem", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

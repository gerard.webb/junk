# envoy

can do:
- grpc
- tls
- global lb
- jwt

Just need to get it running :)

TLS:
"type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.DownstreamTlsContext"

Istio: https://istio.io/latest/docs/reference/config/networking/gateway/#ServerTLSSettings

we are running it as a edge proxy: https://www.envoyproxy.io/docs/envoy/latest/configuration/best_practices/edge

can do TLS: https://www.envoyproxy.io/docs/envoy/latest/intro/arch_overview/security/ssl

or with istio on docker compose: https://istio.io/v1.0/docs/setup/consul/
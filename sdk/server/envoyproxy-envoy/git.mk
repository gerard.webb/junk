



include ./../../root.mk




this-print:
	@echo LIB_REPO: 			$(LIB_REPO)
	@echo LIB_REPO_FSPATH: 		$(LIB_REPO_FSPATH)

this-git-clone:
	$(MAKE) git-clone-args LIB=$(LIB_REPO)/envoy LIB_FSPATH=$(LIB_REPO_FSPATH)/envoy


this-git-delete:
	rm -rf $(LIB_REPO_FSPATH)



this-vscode-add:
	# copy vscode debug json into project
	mkdir -p $(LIB_REPO_FSPATH)/.vscode
	cp ./launch.json $(LIB_REPO_FSPATH)/.vscode/.

	# copy make file in
	cp ./makefile $(LIB_REPO_FSPATH)/.

	# Now add the project as a workspace
	code --add $(LIB_REPO_FSPATH)


this-vscode-open:

	# open vscode
	code $(LIB_REPO_FSPATH)
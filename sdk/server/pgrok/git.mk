include ./../../root.mk

this-print:
	@echo 
	@echo LIB_REPO: 			$(LIB_REPO)
	@echo LIB_REPO_FSPATH: 		$(LIB_REPO_FSPATH)
	@echo 

	@echo 
	@echo PGR_NAME: 		$(PGR_NAME)
	@echo PGR_TAG: 			$(PGR_TAG)
	@echo 

this-git-clone:
	$(MAKE) git-clone-args LIB=$(LIB_REPO)/$(PGR_NAME) LIB_FSPATH=$(LIB_REPO_FSPATH)/$(PGR_NAME)
	# get by tag
	cd $(LIB_REPO_FSPATH)/$(PGR_NAME) && git fetch --all --tags --prune
	cd $(LIB_REPO_FSPATH)/$(PGR_NAME) && git checkout tags/$(PGR_TAG)


this-git-delete:
	rm -rf $(LIB_REPO_FSPATH)



this-vscode-add:
	# copy vscode debug json into project
	mkdir -p $(LIB_REPO_FSPATH)/.vscode
	cp ./launch.json $(LIB_REPO_FSPATH)/.vscode/.

	# copy make file in
	cp ./makefile $(LIB_REPO_FSPATH)/.

	# Now add the project as a workspace
	code --add $(LIB_REPO_FSPATH)


this-vscode-open:

	# open vscode
	code $(LIB_REPO_FSPATH)
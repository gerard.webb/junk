package main

import (
	"fmt"
	"runtime"

	minio "github.com/minio/minio/cmd"
)

// StartMinio -
func StartMinio() {
	minio.Main([]string{
		"--address localhost:9001",
		"server",
		"/tmp/server",
	})
}

func main() {
	//go StartMinio()

	// keep it running
	go func() { StartMinio() }()

	runtime.Goexit()

	fmt.Println("Exit")

}

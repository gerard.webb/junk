# seaweedfs

# Meta data 
Exposes the file system and its authz, changes as a grpc api
And has a golang server for it.

Proto:https://github.com/chrislusf/seaweedfs/tree/master/weed/pb
Server: https://github.com/chrislusf/seaweedfs/blob/master/weed/pb/grpc_client_server.go

SO we expose that to the Flutter layer as a sort of Google Drive meta data

Then the Flutter client and then the Flutter client can display a GSUITE like interface.
But TO get an actual image file for anything ( chat, etc ), the client just requests it using a curl like Http request.

# Client

https://github.com/linxGnu/goseaweedfs
- looks clean and easy
- Could easily build a Dart client that does the same i think.
- ? How to get a list of files in a folder in order to build a Google Drive ? DOnt see this in his API.

How to subscribe to file changes ?
Its here: https://github.com/chrislusf/seaweedfs/blob/master/weed/command/watch.go

https://github.com/seaweedfs/messaging-client-go is for generic messaging, so not it

## Filer API

https://github.com/chrislusf/seaweedfs/issues/1327
- Looks like it will soon be HA !!!

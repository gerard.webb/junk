
# blob

We have to handle blobs and dont right now.

The Flutter client and the Golang Server will need to use it.
Also for Orgs Desktop they will need it running as a Service.


https://github.com/gostor/awesome-go-storage


https://github.com/geohot/minikeyvalue
- NICE and Simple 
- Can replace NGINX, with CADDY, and embed as a Module.

---

The follow steps are:

Run a minio Server

Test a minio Server
- See: https://github.com/brimsec/zq/blob/master/zio/detector/s3_test.go

Provide Token based access to Minio directly for flutter
https://github.com/minio/minio/blob/master/docs/sts/opa.md
https://github.com/minio/minio/tree/master/docs/sts

Need to Upload image
- flu: image picker: https://github.com/flutter/plugins/tree/master/packages/image_picker
- flu: Minio API: https://github.com/xtyxtyx/minio-dart
	- can upload directly then :)

Need to store upload event:
The CRDT Log using IPFS can store it for the event steam.

Need to store it on server:



Then Materialise it into Minio for usage by everyone

- Need to do a few transforms on images
	- Make a blurhash of it. Need this so UI does not blow up from large images.
		- go https://github.com/chuhlomin/go-blurhash-experiment/blob/master/go.mod
		 - flu: https://github.com/fluttercommunity/flutter_blurhash




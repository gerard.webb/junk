
# needed cars
LIB_NAME=		jorm
LIB=			github.com/gioapp/$(LIB_NAME)

# calculated
LIB_FSPATH=		$(GOPATH)/src/$(LIB)

# help

# Note assumes AWK is installed

.DEFAULT_GOAL       := help
HELP_TARGET_MAX_CHAR_NUM := 20

HELP_GREEN  := $(shell tput -Txterm setaf 2)
HELP_YELLOW := $(shell tput -Txterm setaf 3)
HELP_WHITE  := $(shell tput -Txterm setaf 7)
HELP_RESET  := $(shell tput -Txterm sgr0)


# Print help
help:

	@echo ''
	@echo 'Usage:'
	@echo '  ${HELP_YELLOW}make${HELP_RESET} ${HELP_GREEN}<target>${HELP_RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${HELP_YELLOW}%-$(HELP_TARGET_MAX_CHAR_NUM)s${HELP_RESET} ${HELP_GREEN}%s${HELP_RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## Print
print:
	@echo LIB_NAME $(LIB_NAME)
	@echo LIB $(LIB)
	@echo LIB_FSPATH $(LIB_FSPATH)

## Git clone
git-clone:
	git clone https://$(LIB) $(LIB_FSPATH)

## Git clone with args
git-clone-args:
	@echo LIB: $(LIB) 
	@echo LIB_FSPATH: $(LIB_FSPATH) 
	git clone https://$(LIB) $(LIB_FSPATH)

## Git clone ssh with args
# Used for forks of upstream repos.
git-clone-ssh-args:
	@echo WHO: $(HOST) 
	@echo REPO: $(ORG)
	@echo LIB: $(REPO) 

	# e.g: git clone git@codeberg.org-gerard.webb:gerard.webb/beep.git $(GOPATH)/src/codeberg.org/gerard.webb/beep
	git clone git@$(HOST)-$(ORG):$(ORG)/$(REPO).git $(GOPATH)/src/$(HOST)/$(ORG)/$(REPO)


## Git delete
git-delete:
	rm -rf $(LIB_FSPATH)

## Git delete with args
git-delete-args:
	@echo LIB_FSPATH: $(LIB_FSPATH) 
	rm -rf $(LIB_FSPATH)

## Code open
vscode-open:
	code $(LIB_FSPATH)


## golang
go-build:
	cd $(LIB_FSPATH) && go build -o $(PWD)/bin/$(LIB_NAME)

go-build-debug:
	# See: https://medium.com/@tsuyoshiushio/development-environment-for-go-lang-ede316d4512a
	# Set your launch settings correctly !
	cd $(LIB_FSPATH) && go build -gcflags="all=-N -l" -o $(PWD)/bin/$(LIB_NAME)

go-run:
	open $(PWD)/bin/$(LIB_NAME)

GIO_BIN_GLOBAL=gogio

## Build mobile
go-build-mob:
	
## Build web
go-build-web:

# #docker

## Docker up
dock-up:
	cd $(LIB_FSPATH) && docker-compose up -d

## Docker down
docker-down:
	cd $(LIB_FSPATH) && docker-compose down

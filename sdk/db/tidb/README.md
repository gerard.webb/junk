# tidb

Binary based: 
- good for on desktop.
- Need to work out how to run as a service on all desktops with the flutter GUI app, and to package and update ALL together

Docker and docker swarm/ stack based.
https://github.com/pingcap/tidb-docker-compose

Features

Security at rest
- https://docs.pingcap.com/tidb/dev/encryption-at-rest
- Yes we are ok.

Can we use TiFLash to replace MQ ?

https://docs.pingcap.com/tidb/stable/tiflash-overview

https://docs.pingcap.com/tidb/stable/use-tiflash

- It tals to TIKV, and is read only, so its really designed for Materialised Views.

---

Can we use TIDB for file storage ?

NO

https://github.com/tikv/client-go/


https://github.com/baurine/tiup-ui/blob/master/cmd/main.go#L9
- Management GUI
- github.com/pingcap/tiup/pkg/cluster/spec

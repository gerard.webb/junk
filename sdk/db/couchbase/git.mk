include ./../../root.mk

this-print:
	@echo LIB_REPO: 			$(LIB_REPO)
	@echo LIB_REPO_FSPATH: 		$(LIB_REPO_FSPATH)

this-git-clone:
	$(MAKE) git-clone-args LIB=$(LIB_REPO)/gocb LIB_FSPATH=$(LIB_REPO_FSPATH)/gocb
	# get by tag
	#cd $(LIB_REPO_FSPATH)/gocb && git fetch --all --tags --prune
	cd $(LIB_REPO_FSPATH)/gocb && git fetch -t 
	cd $(LIB_REPO_FSPATH)/gocb && git checkout tags/$(LIB_TAG)


this-git-delete:
	rm -rf $(LIB_REPO_FSPATH)



this-vscode-add:
	# copy vscode debug json into project
	mkdir -p $(LIB_REPO_FSPATH)/.vscode
	cp ./launch.json $(LIB_REPO_FSPATH)/.vscode/.

	# copy make file in
	cp ./makefile $(LIB_REPO_FSPATH)/.

	# Now add the project as a workspace
	code --add $(LIB_REPO_FSPATH)


this-vscode-open:

	# open vscode
	code $(LIB_REPO_FSPATH)
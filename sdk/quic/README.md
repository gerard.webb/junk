# quic

explainer: https://github.com/wicg/web-transport/blob/master/explainer.md

It replaces websockets and webrtc fully.

In golang we can do this now and get massive perf increases and developer productivity.

Can i use in Browsr ?
https://caniuse.com/#feat=http3
- IOS and Safari
- Chrome and FF with flag
- Edge not yet but i expect soon.

## Browser debugging

Sites to test with:
https://gf.dev/http3-test

Sites that runs quic:
https://quic.rocks:4433/

Chrome:
	- Toggle: I needed to turn on both
		- chrome://flags/#enable-quic
		- chrome://flags/#dns-httpssvc
		- /Applications/Google\ Chrome\ Canary.app/Contents/MacOS/Google\ Chrome\ Canary --enable-quic --quic-version=h3-27
	- Stats: chrome://net-internals/#quic
Firefox:
	- Togle: Firefox

Set network.http.http3.enabled to true in about:config

## Libs

https://github.com/libp2p/go-libp2p-quic-transport
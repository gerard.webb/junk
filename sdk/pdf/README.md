# pdf

We need to support PDF as an output.

- From a flutter page --> PDF
- From a flutter page --> PDF template
	- the idea is to allow them to create flutter widgets that represent their Personalised Letterhead etc, and then reuse it. Like any CMS would
- From a PDF Template , add data and make a new PDF.

MUST be able to run inside WASM and Native !!!

If we make the Flutetr GUI driven from markdown or xml or whatever its then easy to write a layer to parse this and make the PDF :)


pdfcpu
- https://dev.to/wcchoi/browser-side-pdf-processing-with-go-and-webassembly-13hn
- pure golang and can compile to WASM and so can call from flutter


https://github.com/signintech/gopdf
https://github.com/signintech/gopdf/blob/cc200593af1dd903e551be28c37bc7efc1da00dc/gopdf.go#L688
- has templates !!
- samples
	- https://github.com/oneplus1000/gopdfsample
		- very good samples and no shit license


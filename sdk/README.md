# product

We want to make it easy for new users to add new modules.
We want them to be low skilled and still to be able to use it.

This means we dont use GRPC / Envoy, but can still use Protobufs.

We only plan to support Web and installed we apps. Why ?
- For video conf the only way to do it is to use the browser webrtc ( ion on mobile and desktop essentially failed due to complexity and OS roadblocks)
- Desktop browsers and Mobile browsers all now support installing the PWA onto your desktop, and it works. Users just need a message on the screen to tell them what button to click and its installed onto their device.
	- Called "Add to Home Screen"
	- See: https://stackoverflow.com/questions/50332119/is-it-possible-to-make-an-in-app-button-that-triggers-the-pwa-add-to-home-scree/50356149#50356149
- We dont have to do all the Dev and Ops work to make sure the Native apps work and are deployed to the freaking app stores.
- Its easier for users to try and use the system. 
- Push Notifications do work still.
	- https://caniuse.com/#feat=push-api
	- https://developers.google.com/web/ilt/pwa/introduction-to-push-notifications
		- Web Push is for only the Service Worker, and so for the server to send data to the client
		- Notification is only for the User.

## Topology

1.  Federation Server

These allow the user to login and to enroll in an Org.
This is public and a domain name.

It does not need to do any async operations and is a simple server doing RPC.

Module Registry: WASM modules built by anyone are listed here.

Technically:

Caddy2
- simple proxy with automaitc HTTPS. See the pluigns that allow HA:https://github.com/caddyserver/certmagic/wiki/Storage-Implementations
	- TLS: https://github.com/pteich/caddy-tlsconsul
		- setup 3 digital oceans for it.
	- Auth: https://github.com/greenpau/caddy-auth
	- others: https://caddy.community/t/list-of-caddy-2-modules/7839

GRPC / Protobufs
- Golang Server and golang WASM client = no state controlled by Flutter and fast development and runtime.
- Client 
	- Webworkers can make network calls & have access to IndexDB. Use Comlink ( flutter wrapped ) to make life easy : https://github.com/GoogleChromeLabs/comlink
- Server
	- GRPC code gen for Server and Client.
	- https://github.com/dave/protod
	- https://github.com/dave/groupshare

Auth with oAuth: https://github.com/matthewhartstonge/hasher
- oAth, JWT, Password management on in one: https://github.com/matthewhartstonge/hasher/issues/3
- We might need this to allow third parties to use

RPC Async: https://github.com/Cretezy/dSock
- Multi user web sockets gateway using Redis, instead of NATS

RPC Sync: https://github.com/lithdew/flatend
- Multi user http gateway with UDP based push to Org Relay Servers over special UDP proto
- This can also do AUtoCert for HTTPS, and so allows the Org Relay Servers to have a simple way to boostrap without needing Caddy.
- Modules can be easily written with very basic config. See Examples.

Messages BASE: https://github.com/liftbridge-io/liftbridge
- Solid message store via GRPC local

BLOB Base: https://github.com/minio/minio-service/tree/master/windows
- Solid Blob store

RPC GUI Flutter: https://github.com/Cretezy/flutter_super_state_example
- simple pattern for each module to get messages and update client state
- We will add persistent store as needed.
- The Kappa computation happens on the Org Server, so the client just updates its own GUI views.

OPS
- Client: https://github.com/appspector/flutter-plugin
- Server: not sure yet.




2. Org Server

This is a Server ( many in HA mode in different locations) that holds all data and logic.
It is NATed, and so not reachable normally on the internet.

Relay: This Server is to act as a Relay server in that all messages and blobs are sent to it.
This is needed because all parties might not be online at the same time and so it acts as a global message queue relay, holding the data until all parties have recieved the data.

Modules. Held here to be used by anyone. Public or Private.


3. Devices

A Users device ( mobile, desktop, web)

Note that on Desktop we should install a golang server because
- Most users wont run their own rasp PI Server unless its a big Org and so at least they can just run a client
- The GUI still runs as a Flutter Web PWA. We need to use MakeCert to install a fake certificate to trick the PWA into trusting the Notification messages from the local golang server.

These talk to the Federation Server and the Org Server to do all communication.
They hold all data Materialised based on a Kappa Architecture. All data eventually forms via the messages flowing through the Org Relay Server.



## Storybook.

Allow flutter pages with widgets and content to be laid on a canvas with lines between them representing user flow.

- Maybe use markdown or mermaid etc to represent the flow.
- This is needed anyway if we want to really be a CMS because the flow needs to be described this way. I bet it's possible using wasm and golang with a golang CMS style lib.

https://github.com/rodydavis/storyboard
- Works but does not show the navigation paths for buttons, etc.

## ERD

We need an Entity Relationship diagram.

- A way of modeling data and then generating code from it.
- Looks like a typical database ERD tool.
- Exposes the API via a Data Source API
- Is 100% Kappa based in that mutation messages ( protocolbuffers ), describe all changes, Tables describe all Views, and Mutation Commands describe all state changes to the Views as a result of the Messages.


Technically:

- Server runs a CRDT Log to give all message HA: https://github.com/StreamSpace/ants-db
	- IPFS-lite node that stores Messages and blobs for everyone.
- Client runs the DB that is the Mutation Events, Commands and finally the Aggregates ( to make the Materialised Views ) that are eventually consistent: https://github.com/mishudark/triper
- Client runs the Documents and Blob store: https://github.com/yorkie-team/yorkie


## Modules

Use the Storyboard and ERD to complete a Modules.

- Compiled to WASM running in our Runtime.
- Server side runs on Org Server
- Client side runs on Devices.

# markdown to PDF

All docs are written in Markdown 

Basis
- Gitea: https://github.com/go-gitea/gitea/tree/master/docs
	- has localisation
		- we can use out tool to do it automatically i am sure.
	- standard docs dir
		- So use this standard for ANY repo
	- publish
		- Google Cloud storage
		- Cloud Function with HTTPs ? https://medium.com/google-cloud/how-to-run-a-static-site-on-google-cloud-run-345713ca4b40

So great, but now we need a way to publish to:
- PDF For the App
- Web: for Hugo

- HTML --> PDF: https://github.com/unidoc/unipdf
	- control it all with golang and nothing else.
	- HUGO, we get control
	- APP: embed the golang, or run on server ( their or owns)

- MARKDOWN -- > PDF: https://github.com/hayajo/md2pdf
	- works fine

- drawings, excel ,etc etc
	- We just use GO PDF, and run it off JSON.
	- Flutter has its own modules


## todo

Integrate into core-bs and core-fish, from their releases: https://github.com/wkhtmltopdf/wkhtmltopdf/releases/tag/0.12.5



module codeberg.org/gerard.webb/junk

go 1.14

require (
	github.com/JohannesKaufmann/html-to-markdown v1.1.0 // indirect
	github.com/markbates/pkger v0.17.0 // indirect
	github.com/mkideal/cli v0.2.2 // indirect
	github.com/shurcooL/go v0.0.0-20200502201357-93f07166e636 // indirect
	github.com/shurcooL/go-goon v0.0.0-20170922171312-37c2f522c041 // indirect
	github.com/shurcooL/goexec v0.0.0-20200425235707-36ff6d2d1adc // indirect
	github.com/suntong/html2md v0.0.0-20200727031908-c3106da39875 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/tools v0.0.0-20200807202252-a5d4502270e7 // indirect
)

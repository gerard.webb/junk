# crypto


Hybrid crypto
https://cfrg.github.io/draft-irtf-cfrg-hpke/draft-irtf-cfrg-hpke.html
- code
	- https://github.com/cisco/go-hpke


MLS is the open standard
- spec: https://github.com/mlswg
- code:
	- https://github.com/cisco/go-mls
		- nice golang code.
		- uses:
			- https://github.com/cisco/go-tls-syntax


DNS without eaves dropping
https://github.com/chris-wood/odoh-server
- uses: github.com/bifurcation/hpke (https://github.com/cisco/go-hpke)
- players: https://github.com/bifurcation


# ssho tools

# private vars
SSH_FSPATH=~/.ssh/

## Prints the OS settings
ssh-print:
	@echo
	@echo -- SSH --
	@echo Assumes your on Mac or Linux. Windows is a PITA and not supported
	@echo SSH_FSPATH = $(SSH_FSPATH)
	@echo

ssh-test:


	$(MAKE) ssh-create-withargs SSH_NAME=foo.bar@example.com

## List your ssh config
## SSH-FList (Lists all ssh key in the folder)
ssh-folder-list:
	ls -al ~/.ssh/

## SSH-Create (Creates an ssh key)
ssh-create-withargs:
	@echo SSH_NAME is $(SSH_NAME)

	
	# Make a new key
	#ssh-keygen -t rsa -b 4096 -C "$(SSH_NAME)"
	#ssh-keygen -t rsa -f ~/.ssh/id_rsa
	#ssh -p$SSH_PORT -q joker@$INSTANCE_IP 'yes y | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa'

	ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa_$(SSH_NAME)

	# List result
	ls -al ~/.ssh/*$(SSH_NAME)


## SSH-Agent-add (Adds an ssh key to the agent)
ssh-agent-add-withargs:
	@echo SSH_NAME is $(SSH_NAME)

	# Add ssh key 
	# ssh-add ~/.ssh/winwiselyXXX_github 

## SSH-Delete (Deletes an ssh key from the agent)
ssh-delete-withargs:
	# Delete ssh key (if you screw it up)
	# ssh-add -d ~/.ssh/winwiselyXXX_github 

## SSH-List (Lists all ssh key in the agent)
ssh-list:
	ssh-add -l 
	

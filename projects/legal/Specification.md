# Legal Software

Base architecure with the following on top.

## Base Architecture summary

- Orgs and Projects directory
    - Allows Federated Teams that are inter and extra to your Org to have access to the system.
    - Use Projects to apply different security policies for Internal and External actors / people.
- Secure login with various forms of 2FA ( 2 Factor Authentication).
    - FIDO2 and WebAuthn 
    - SMS but its really not at all secure. 
- User Authentication can be conncted to LDAP, Active Directory or OIDC servers. 
    - Many existing large multi-nationals use these systems to control user accounts and also rights.
- Fully encrypted.
    - Database is fully encrpted. 
        - Master key can be stored on a personal TPM ( e.g yubikey or Solokey). 
        - Anyone person with physical access or theft gets nothing.
        - Any intrusion via malware or personal getting Phished ( typically via their email account) is not going to compromise the software.
    - Network encrypted.
        - TLS 1.3 
        - ESNI
        - ECHI ( future standard, but not yet )
    - DNS Encrypted
        - Anyone using the system is 100% untracable. 
        - Normally with End to End encryption systems ( Telegram, Whats app, Signal), you leak to the Government and the ISP that your are using the system and they collect meta data on your usage and geographical position, and the trace route of the path from you to the server. That easily allows them to pinpoint who you are.
    - Can Run fully internal on Premise or in the Cloud.
        - When run internally, you can shut of external netwok access if needed.
    - All Real time communication employs the same security standards as the gold standard which is Signal as used by Governments, etc.
        - This is called MPL or double ratchet in the industry. 
        - But with No IP Leakage. Despite all the above security measure, Signal, Whats App, Google Meet all leaked your IP address. This is because they use WebRTC as the communication means. WebRTC works by both paties within a real time communication session sharing with each other their IP address, in order to do a Peer to Peer session. We do not allow this. We force all real time communciation ( data, video, audio ) to be relayed via our Server, and thus each users cannot discover any other users IP address. 
        - Also: Finding the IP address of another user when using Signal and other similar WebRTC software is just a matter on installing a proxy sniffer on your machine. Its not at all hard, is common software and someone can be shown this in 30 seconds.
        - Also: The WebRTC protocol forces connections to be opened on your router. Once these are opened, malicous actors can exploit it.
- Run on Web, Desktop and Mobiles.
    - Power users need to be able to use the system any time and from any device.
- Can run offline.
    - This is important for people working in countries where network access is not high quality.
- Providence and Audit trailing (Compliance)
    - Providence means that the source of anything is recorded.
    - Audit Trailing is a log of what changes occurred in the system. 
    - These Compliance aspects are useful if you have any malpractice internally. They are needed to conform with ISO standards and EU and US Government standards.
- Issue Kan Ban
    - All changes in the system can be linked back against an Issue.
    - This is a type of aduit trailing, but is designed to allow all users to collaborate around Issues and work collectivly to resolve them, whilst keeping an Audit trail at the Issue level. 
- Secure Email and DNS
    - The system incorporates a DNS and Email Server.
    - This is needed by some Orgs that are using third aprty email and DNS, and so leaking their and their clients privacy.
    - The DNS Server acts as a type of Applications firewall in that it prevents malware, bots and other external tracking systems to reach the main servers or any of your users. 
    - If they all use the DNS Server they get this protection. 

## Legals Teams summary:

Problem Statement:

The legal software deals with the problem of languages and multimedia.

Various documents, audio or video may not be in the Legal Teams language. 

Managing this huge amount of documenation is a hugly inefficient for the Legal Team. You cant work concurrently, and you are tied to the physical location where the documents exist.

Solutions:

Use the Project functionality for each case.

Scan all the Documents and index them, so that all the Team can concurently work on the case from anywhere.

Transcribe all documents into the Legals Teams lanuguage and also other actors languages.


The following functionality is provided.

1. OCR

All the Deposition documents from all over the world in various languages is canned into the sytem. 

The system then translates them to English or to most other languages ( ONly unicode a the moment ).

Indexes the documents.

Provides facets to be applied, to allow staff to do facet search.

Why ?

- Legal teams often have projects that have actors involved that are overseas or in the same country but are immigrants and do not speak the naive language.

- These can be any actors - Extra jurisdiction cases, or even just immigrants in the same country.

- Documents ( Testimonials, contracts, wills, etc, etc )

2. Audio

Same as OCR but applied to any Audio.

These are translated and also transcribed to text, so it can be indexed and then searched against as needed.

Why ?

- Part of a case can involve audio files.


3. Video Conferencing

Same as OCR but for Video conferencing.

In a 2 person or many person chat or video conference it translates the chat text and the audio to everyones own languages in real time.

The audio is recorded and transcribed to text as a log, so that the machine translated text can also be checked quickly by a human within 5 or 10 minutes. They can be notorised or not. Up to the Team using the software.

Why ?

- Often parties are not all speaking the same language, and you dont have a notorised translator around.

- You need to do due diligence quickly in real time. Actors can be anywhere around the world, and a fully encapsulated secure system is the best option.

- You need it to be 100% secure. Most other systems are actually not.
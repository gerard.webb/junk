# medical stuff

If we are doing medical then if we support FHIR it gives us a fighting chance to talk to the backends.

## FHIR
https://github.com/google/fhir/pull/6
- soon will have golang protobufs !
- But if you want to use them without lots of complex code you need to be able to store protobufs in a DB !

## Gaia-X Health

https://www.data-infrastructure.eu/GAIAX/Navigation/EN/Home/home.html


## DICOM

Easy

## Google cloud
https://cloud.google.com/healthcare
- can also run on this with SAME API probably as the protos 

stores
https://cloud.google.com/healthcare/docs/reference/rest/v1/projects.locations.datasets.fhirStores#FhirStore
based on: https://github.com/FHIR/sql-on-fhir/blob/master/sql-on-fhir.md


genji !!
This really looks like a good fit !!
https://github.com/genjidb/genji
- run on mobiles and servers
- SQL API over JSON documents, and so also protobufs !!!!
- Can be used as CRDT store
- has WASM :) https://github.com/genjidb/genji/search?q=wasm&unscoped_q=wasm
	- The JS binding is here: https://github.com/genjidb/genji.js
	- MY lucky day !!
- has CDC ? 
- docs
	- How he does it is Very interesting
		- https://github.com/genjidb/genji/tree/master/docs
	- Uses Lunr.js for offline indexes and it works
		- https://github.com/genjidb/genji/blob/master/docs/config.toml#L110
		- there is a golang versin: https://github.com/riesinger/hugo-golunr
			- so i can wasm it or use native.

Search
This searches a git repo and makes indexes.
https://github.com/JakeHendy/Dockerfiles/blob/master/zoekt/Dockerfile


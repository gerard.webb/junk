# Archi-nats

Why ?

Has its own JWT Server and Store.
Can do messaging and RPC.
Has Websockets
- SO no need for grpc or envoy
- BUT needs the nats.go to support golang websockets and compile to WASM ( prefer tinygo)
Can do Protobuf
- SO still ok for different versions
EASY to understand
- Its simpler.

Get and PUt images happens by chunking them to max of 512KB ( half a mb), which is enough

Flutter
- Now we can embed the golang and expose it using FFI
- Then Flutter just does Views and data calls and the golang embed handles all the other stuff like
	- Data calls
	- File upload, download
	- Security ( enforced by NATS Server itself )
	- Database using Genji.





Can use Caddy for TLS.
- Easy and works

Want to embed a Client in flutter anyway
- SO we need a golang lib that can be compiled to WASM

nats.go working in browser ?
- Server
	- NATS Server support Websockets now: https://github.com/nats-io/nats-server/issues/315


- Client 
- need to support WebSockets so that we can cross compile it for Web: https://github.com/nats-io/nats.go/issues/577
	- I added that we could use https://godoc.org/nhooyr.io/websocket

https://github.com/nats-io/nats.go
- What does it use that we can swap out to make it use a websocket instead 

https://github.com/simpleiot/simpleiot
- nice wrapper
- Has Twillo integration ( https://github.com/kevinburke/twilio-go), so can do SMS and Voice Messages.
	- For 2FA Security.
- Nice APi that matches our needs nicely: https://github.com/simpleiot/simpleiot/blob/master/docs/api.md






# archi 05 - webrtc proxy all the way through


Use grpc but use improbable and webrtc.
Still use Protobufs and the grpc IDL to drive it.

JWT and Sec
- GOA Security has working example :)
- https://github.com/goadesign/examples/tree/master/security

GRPC with WASM and golang and no envoy
- many candidates

WASM brotli compression.
- https://github.com/ctessum/cityaq
- works

PDF WASM works
- https://github.com/wcchoi/go-wasm-pdfcpu/blob/master/article.md


GraphQL, and not grpc
github.com/dosco/super-graph/core




Prob: Envoy and k8 layers just get in the way and cause big issues for users deploying this.
- We just have too many layers and transports required and it slows us down too much.
- Orgs need to run a private Server on any Server, Desktop or Rasp PI and envoy / k8 is getting in the way !!
- We need to run a public Relay Gateway on standard servers without the insane complexity of k8, etc
- SO instead:
- Use Caddy v2 and its REST based Control plane. No magic and we get control back.
- Has auto Certs with storage into Consul. Solves the Orgs problem.
- Use Consul for Service Discovery across all Org and Our Servers.

We can still deploy to K8 for scaling big. THis shows how to do it:
- http://www.doxsey.net/blog/kubernetes--the-surprisingly-affordable-platform-for-personal-projects
- But the key is to NOT use all the other bits of K( so you can run on k8 and NOT on k8 !!
- Handle Certs ourselves.
- Handle Ingress ourselves.
- Handle All Telemetry ourselves
- Handle LB ourselves.

Prob: GRPC and codegen are needed for sure, but the transport is wrong.
- Sol: Bring in the Embed layer we always wanted to bring in from the start.
- We know we need to do P2P and Client to Server, so then lets use WebRTC as the global transport for everything. Both Client / Server and Client / Client.
- WebRTC will be soon be replaced with QUIC Web Transport and so the two are the same protocol semantics, and so it will be very easy to support both WebRTC and QUIC. The Ion project already does it.
- SO Instead:
- Get the GRPC code gen working using Improbable GRPC backend using WebRTC.
	- https://github.com/jsmouret/grpc-over-webrtc
- The Client proxy ( that lives inside the client that we call the "Embed") is golang that can then be embedded with the DB and other security enforcement things we need, and so can do the complex things that Dart cannot. We can cross compile that client to WASM, Desktop and Mobile easily and call it all from Flutter. Standard stuff and what we wanted to do all along.
- Mobile call via JS, Desktop call golang to golang, Mobile call via FFI or Method Channels.
- We can also use Protobufs for the Flutter Client <--> Client Proxy layer, and code gen the JS and FFI code for the client. 

Prob: Video and Video Conf does not work everywhere.
- Video
	- Only works on Web and Mobile native
	- So for Desktop use ZSerge Webview container from go-flutter, and open the video in a New window that laod Flutter Web.
	- Host all on Youtube. There is a Flutter Plugin that plays Youtube with no restrictions.
- Mobile Ion works sort of.
- Desktop does not exist. Native webview does work but not on Mac or IOS. ONly option is to use Electron and not sign the code. Will be an issue later, Apple have blocked Webview access.
- Web does work.
- Option1 : Use GIO for this part. But not at all ready yet.
- Option 2: J

Note we could then move to pure GIO GUI.
- way less layers.
- webrtc and video does not yet work on desktop, mobile or web yet. Web will require a Dom div tag that we flip on and off. Desktop and mobile could do the same with a webview. Then it's 100% golang !

## Layers
Client embed: gen the code
- for client. Use standard dart protobufs to gen the dart code.from proto
- for server proxy
Server proxy: gen the code from where ?
- for client proxy
- for server
- can do async handoff to NATS if needed 
Server : grpc IDL is here !
- standard grpc golang here.


Improbable does support websockets.
https://github.com/improbable-eng/grpc-web/issues/94

Ex: https://github.com/johanbrandhorst/grpcweb-example
- he embeds I side the golang a go generate. Best way !
- we would then generate what we need for the calling client at the 2 levels.
- the gophers client should be easy to make work with WASM because golang compiler does it all for you these days.

Ex:
https://github.com/nhooyr/websocket/issues/251
- he got it working 
- is using improbable backend.

Search: 
grpcweb.WithWebsockets(true)

Vugo
https://github.com/vugu/vugu/issues/142

---

So fuck all the above !!! and all the shit people are hitting.
There are so many fucking issues with websockets and Google grpc and envoy are interested in quic.

Webrtc for client server using grpc IDL.
- We need Webtc For P2P anyway, so use it for Client / Server
- Pion/Ion uses a version of it with out Protobufs already and we are wedded to them for Video


Pion has both and tons of examples so use it.

Ex: 
https://github.com/jsmouret/grpc-over-webrtc
- client is TS

Pion webrtc data channels supports WASM. https://github.com/pion/webrtc#webassembly
From: https://github.com/dennwc/dom/issues/10

WEBRTC WASM ex
- https://github.com/pion/webrtc/tree/master/examples/data-channels

# https://github.com/spatialmodel/inmap

# Fork: https://github.com/joe-getcouragenow/inmap


LIB_NAME=		inmap
LIB=			github.com/spatialmodel/$(LIB_NAME)
LIB_FSPATH=		${GOPATH}/src/$(LIB)

GIT_UPSTREAM_URL=	https://$(LIB)
GIT_FORK_URL=		git@github.com-joe-getcouragenow:joe-getcouragenow/$(LIB_NAME).git

git-print:
	@echo
	@echo -- LIB --
	@echo LIB_FSPATH: 	$(LIB_NAME)
	@echo LIB: 			$(LIB)
	@echo LIB_FSPATH: 	$(LIB_FSPATH)
	@echo

	@echo -- GIT --
	@echo GIT_UPSTREAM_URL: 	$(GIT_UPSTREAM_URL)
	@echo GIT_FORK_URL: 		$(GIT_FORK_URL)
	@echo

git-upstream-clone:
	# Upstream
	git clone https://$(LIB) $(LIB_FSPATH)

git-fork-branch:
	# hardcoded to "master"
	cd $(LIB_FSPATH) && git fetch --tags && git checkout master

git-fork-clone:
	# Fork
	git clone $(GIT_FORK_URL) $(LIB_FSPATH)

	# Add link back to upstream
	cd $(LIB_FSPATH) && git remote add upstream $(GIT_UPSTREAM_URL)

git-fork-status:
	cd $(LIB_FSPATH) && git remote -v
	cd $(LIB_FSPATH) && git status
	
git-fork-catchup:
	# Get changes from upstream into my local fork
	cd $(LIB_FSPATH) && git fetch upstream
	$(MAKE) git-fork-status
	
git-fork-push:
	# Commit to my github fork, so i can then PR upstream
	cd $(LIB_FSPATH) && git push origin

git-delete:
	rm -rf $(LIB_FSPATH)

vscode-add:

	# copy vscode debug json into project
	mkdir -p $(LIB_FSPATH)/.vscode
	#cp ./launch.json $(LIB_FSPATH)/.vscode/.

	# copy make file in
	cp ./../makefile $(LIB_FSPATH)/.

	# Now add the project as a workspace
	code --add $(LIB_FSPATH)

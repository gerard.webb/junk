archi notes

We need certain things on the backend and have have time to do oursleves.

We need a system that can allow a client ( mobile, desktop and web) to BE the Event Log. Public will want this.
But also the same where a server is one. Corps and Orgs will want this.


# Basic

- Change code namespace
Namespace code to a domain to be independent of the git repo namespace
- https://gopkg.in

Add golang obfuscation.
- https://github.com/search?l=Go&q=mvdan%2Fgarble&type=Issues
- I think it can codegen the goalng code, and so then we can run it into gomobile, etc


Get Github doing the CI for ou code on Codeberg !!!
- https://github.com/mvdan/github-actions-golang#how-do-i-install-private-modules
- The other instructions there are good too.


## hugo stuff

https://github.com/abiosoft/caddy-git
- Will deploy baaed on git hooks and nothig lse.

In fct start using JUST caddy V2 for hositng anything we need to host
- Lots of goodies here: https://github.com/abiosoft?tab=repositories

---


Winner so far !!

https://github.com/yorkie-team/yorkie
https://github.com/yorkie-team/yorkie-js-sdk

- CRDT with MongoDB and protobuf over GORC web ( envoy )
- His is copying the AutoMerge code: https://github.com/automerge/automerge#undo-and-redo
- Its generic in that the JSON doc driven it.
	- See example: https://github.com/yorkie-team/yorkie-js-sdk/blob/master/dist/drawing.html
		- ITS VERY SIMPLE !!
		- has quill: https://github.com/yorkie-team/yorkie-js-sdk/blob/master/dist/quill.html


https://github.com/automerge/automerge-rs
- Yorkie is golang but this is RUST and up to date !
- We know we can use this from flutter and flutter web, so maybe we should use this
- Slack: https://app.slack.com/client/T61MVPYP5/C61RJCM9S
	- gerard.webb@ubuntusoftware.net

https://github.com/jerson/flutter-openpgp
- this and his others clearly show how we can have a golang client embedded in flutter using gomobiel and wasm !!!!


SO with a pure Client DB with CRDT, all we really need on the Server is a signal Server !
- THis is where Ion shines for us


Also by embracing Webrtc and CRDT we satisfy the business needs:
- Persons want a P2P private system, and our servr can hold an encrypted event stream for when they enroll a new device
	- Of we use a animated QR code. 
- Corps can have it all run on their server with NO public IP
	- The CRDT is happening on the Servers as a form of HA replication
	- They still use Webrtc as the network transport, but their server is a relay server.

There is no reason we cant just make the Flutter app ALWAYS use a Webview. Why ?
- Always up to date
- Only ONE thing to worry about
- Just need to solve the zerge webview issue.
	- We know the solution. Just use https://github.com/asticode/go-astilectron, but embed the Framework files


Tunnel
- Rather then webrtc, cant we use QUIC instead
- gomicro tunnel for example
- Wireguard exposes everything, where as QUIC only exposes the Service being used and so is less risky and intrusive.
- BUT ALL of the above leak their WAN IP address !!!

SO then use a Inlets tunnel (https://github.com/inlets/inlets)
- Everything goes through the public server, and so does not leak the users IP
- Simple too using just Https

Inlets can also be used to provision Servers with Terrafrom !
https://github.com/inlets/cloud-provision-example
- inlets can also do cloud provisioning.

Search on: github.com/inlets/inletsctl/pkg/provision

Also openFas gives us a cloud queue based on NATS, and so can allow us to forward the messages for when the Users device is NOT online
- this is not so bad because once delivered we then dont hold the message.

https://github.com/openfaas/faasd
- runs openfas without ANY k8 underneath.
- this means we COULD run this on our cloud as the gateway BUT also on the End users servers ( like doctors)
- IS EASY: https://blog.alexellis.io/deploy-serverless-faasd-with-cloud-init/

https://github.com/openfaas-incubator/ofc-bootstrap
- Gives you CI and Git ops.
https://docs.openfaas.com/openfaas-cloud/intro/

Gloo can control OpenFASS in terms of publishing. This is the perfect way to mnage open fass
https://medium.com/alterway/call-openfaas-serverless-functions-with-gloo-api-gateway-678a00710481



https://github.com/faasflow/faas-flow-minio-datastore
- Gives us Minio and function composition
- We need minio to be plugged in because we also need async delivery of Images across orgs and so need to hold it for them like a message queue holds data messages
	- once delivered we can delete it. 


Tor Network
https://github.com/ipsn/go-libtor
https://github.com/lu4p/ToRat
- If we need Servers we can put them here
- this means we can do rendevous between users via the Tor network !!!!!



---


All Clients blow away all data when they load a new version and replay ALL of it back
Why ? 
- SO we never have to deal with schema evolution
- Added more devices is trival
- Storing Logs is trivial

How to do kapps and images ?

Images are immutable. Yu cant change them
- Storing them requires i think a data payload describing them and the images.
- put in a document/distributed file system and include a
link to it on the event.

https://github.com/go-ocf/cloud
- looks perfect !!
https://openconnectivity.org/developer/specifications/
https://openconnectivity.org/specs/OCF_2.1.1_Specification_Overview.pdf
- Their is a kappa archi




## Basis of decentralised

- GUI
	- IF we want we can run Everything in a webview using flutter webview.
		- Instant updates, and no app store BS
		- GUI is still built with flutter
		- GUI can use modules that are truely seperate. We just maintain a Native flutter GUI, and load the modules as WebView inside.
			- This will allow the thing to truely scale.
			- Any Core stuff can run outside the Modules.


- Boostrap Server 
	- IPFS Cluster will do it.
	- k8 for it: https://github.com/AIDXNZ/KubeIPFS

- gomobile IPFS
	- Berty Team is a fully open NGO and have their shit together
		- Have a gomobile version of IPFS
			- Have bluetooth wrapped for no internet comms
		- https://github.com/moul is a reviewer !



- All mutations are an event that is stored into a Event Log somewhere
	- that MUST be CRDT distributed
	- For a Client we can run a Multi tenant server that is a 2nd device and so be HA in our cloud

	- Search: https://github.com/search?l=&o=desc&q=crdt+language%3AGo&s=indexed&type=Code

	- https://github.com/emitter-io/emitter/tree/master/internal/event/crdt
		- The event store is CRDT and so scales out. ITS LWW, and so does not tolerate partitions, and so will not work for offline
			- for store uses  github.com/tidwall/buntdb
	- https://github.com/dapr/components-contrib/blob/master/state/cloudstate/cloudstate_crdt.go


	- Cloud State
		- https://github.com/cloudstateio/go-support
		- https://github.com/cloudstateio/dart-support


- All users then get the event(s), make a command based on it and then update their materialized views

- Discovery will require WebRTC basis.

- There is NO Center !! THis is key.

- Use WASM to achieve this.
	- Flutter dumb with golang embed running network and storage.
		- Flutter has nothing to do but get update events for its Views, and send mutations to go embed layer.
			- A simple Protobuf
		- https://github.com/bytecodealliance/wasmtime-go
			- Users: https://github.com/search?q=wasmtime-go&type=Code
			- https://github.com/unicredit/mosaic
				- Promising. 
		- https://github.com/wapc/wapc-go
			- Can run on mobile, web server	

Users Keys and groups, so we dont need a third partyfor trust
- MLS is the only game in tonw: https://github.com/mlswg/mls-implementations/blob/master/implementation_list.md
https://blog.trailofbits.com/2019/08/06/better-encrypted-group-chat/
- Cisco have a nice golang one: https://github.com/cisco/go-mls
	- 	





## Security

- Security needs to be like a Facet of:
	- User
	- Role
	- Org, Project
	- Label ( form each project)

Global federation of these Facets need to be maintained somewhere.

- I think it can just be one of our Servers running the same code that is responsible for the Global facets.

- As it changes all clients get the updates, and so maintain that View ?
	- Too much data for mobiles

## Modules

https://github.com/filebrowser/filebrowser
- finally a google drive 


## ci

Rasp PI for everything.
- ci
- web
- production.

Have 6 differnt options for hosting :)

Inlets Server in Cloud: https://www.civo.com/pricing
- https://github.com/inlets/inletsctl/tree/master/pkg/provision

https://wsl.dev/wsl2-k3d-arkade/


## backend

https://github.com/kelindar/talaria
- ingest events and use emitter and crdt to spread load

https://github.com/emitter-io/emitter

https://www.scylladb.com/
- SQL data store with CDC

- Run self hosted at home Or on cloud
- Data is cakcged on Client but no edits allowed offline, which gives good speed but removed all data synchronisation issues
- SO we can just run a standard TB with CDC for updates of cache to clients.
	- Because all mutations occur at the Server then there are no ordering conflict of the change events pushing up to a client
	- Change feed is per Users device, so that their devices can be running at different update rates
	- Because a Client might be offline, we need to collect a Users messages for them for later
		- Its quite possibel to do this in the DB itself IF we want to.
		- Because the TIDB is clustered then with a Global LB then any server in any data center can serve the data. Very HA.
- Presence
	- Only used for where to send notifications.
	- Notifications via Google and Apple: https://github.com/appleboy/gorush#support-platform
	- Can be used for webrtc calls to tell the uer they have a call and then intents route to the actual call.


## Ours

For now lets run a few things on top of other people
- Git: https://codeberg.org/
	- Use FreeOTP for 2FA
		- https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp
		- https://apps.apple.com/us/app/freeotp-authenticator/id872559395
- Drone: https://cloud.drone.io/getcouragenow/
	- currently pointing to github.
- OAuth: run on DO
- Voice and SMS: plivo
	- SO we have a gateway
- Global DNS: Mercury
	- So we can load balance to our own boxes from then on.
	- Requires a LB at each Cloud or Local.
- Wireguard ?
	- https://github.com/tailscale/tailscale
	- https://login.tailscale.com/admin/acls

# Cover with our own Domain

Gitea has a OAuth and so we can use it as our DB for users.
oauth2-proxy can then use it.
SO then we force everyone to use this.



## Ops

TO watch and deploy it all.

https://github.com/chef/automate/blob/master/lib/grpc/auth_context/auth_context.go
https://github.com/chef/automate/blob/master/components/authz-service/README.md
- Chef has a perfect auth Context for Amplify itself
	- Subjects
		- Can be tags of any sort
	- Projects
		- Can be Projects Or Orgs you allowed into.


THis gets us the Core shit we need
- Just put a box at Joe and Rosie house and global its access.

Gitea
- Get off crappy github
- Host on mac ?
	- Inlets

oAuth
- https://github.com/oauth2-proxy/oauth2-proxy
	- Run this and so now we can use Gitea as our Auth Provider
		- code is here: https://github.com/oauth2-proxy/oauth2-proxy/issues/217

SMS and Voice:
- Need to be able to send auth tokens
- https://github.com/plivo/plivo-go

VSCode
- Make it easy and just run it on our own Server
- https://github.com/cdr/code-server#macos


CI
- Circle or Drone ?
- Gitea works well with Drone using webhooks
- https://docs.drone.io/server/provider/gitea/
- SO devs can run CI on their own laptops !!
- Runners: https://docs.drone.io/runner/overview/
	- Exec: https://docs.drone.io/runner/exec/overview/
		- SO just run on devs machines without docker at all.
		- Good for Client builds.
- Cloud and free for open source !
	- https://cloud.drone.io/
		- x86, Arm32 and Arm64 bare metal servers :)
- Local CI can also be make public
	- https://github.com/paurosello/inlets-chart
	- https://github.com/jfelten/gitea-helm-chart
		- docs: https://itnext.io/explore-gitea-drone-ci-cd-on-k3s-4a9e99f8b938
- Telegram drone integration
	- 1. create a bot in Telegram using BotFather
		- Use: https://telegram.me/botfather
		- create new bot and name it: https://t.me/gcndronebot
			- it will give you back the token to the new bot.
			- You can now use this bot and share it with others.
		- Use this token to access the HTTP API. Our new token is: 1048768669:AAED3FNrwEEj9O-SULdRXCszzl8xPOBem5U
		- Check the chat via: https://api.telegram.org/bot1048768669:AAED3FNrwEEj9O-SULdRXCszzl8xPOBem5U/getUpdates
			- notice the works "bot" at the start before the token
- Examples:
	- https://kolaente.dev/vikunja/api/src/branch/master/.drone1.yml
		- Is like our project :)
	- https://github.com/owncloud/ocis-pkg/blob/master/.drone.star
		- uses drone.star which is better 




Minio
- For everything else.
- Hugo 
- Releases, and binaries

Backup
- Backup to google drive perhaps because minio could go down by us.

Telegram
- SO we get notifications when things happen on anythng above.
- https://github.com/requilence/integram
	- Already supports lots of above.

Docs and Web
- Use Dgraphs hugo templates. They are clean and modern 

Diagram
- Use draw.io
- Just save and export to SVG etc for hugo, using make file


## Prdouct

Global Load Balancing
- Need to be able to dirct calls to best data center
	- https://github.com/schubergphilis/mercury
		- AD web login integration has LDAP using: github.com/go-ldap/ldap

Cloud Proxy
- inlets
	- Cloud and Local: https://github.com/inlets/inlets-operator


Local Supervisors
- Caddy Superviser
	- https://github.com/lucaslorentz/caddy-supervidocker stack deploy -c examples/standalone.yaml caddy-docker-demo
	sor
	- Can supervise anything, including the Caddy Proxy
- Caddy v2 looks strong
	- Docker and Docker Swarm Proxy
		- https://github.com/lucaslorentz/caddy-docker-proxy
			- Labels are used inside Docker
			- SO then maybe docker compose it not needed ?
	- Scale using startand Linux with a decent ignition / Cloud Init
	- HA with a TIDB is just easy.

Monitoring
- https://github.com/cloudradar-monitoring/cagent
	- 2 euro / server / month https://www.cloudradar.io/
	- this is actually worthit because monitoring cant be done by yourslef as its a dog chasing its tail.


SDK Packaging
- core-bs repo
	- porcelain layer for bare metal setup
- core-runtime for all common stuff
	- Client and Server
- gofish for binaries https://github.com/fishworks/gofish
	- core-fish repo
- arkade for Helms: https://github.com/alexellis/arkade


Runtime

https://github.com/textileio

https://github.com/textileio/go-threads/blob/master/api/pb/dart/pubspec.yaml
- Provides a local DB using protobufs
	- Model is a Collections KV style API
	- Is generic and so easy to use 
- These are using CRDT from IPDS ?



# archi 06

Use Pion / Ion: https://github.com/pion/ion

Has JWT: https://github.com/pion/ion/pull/303/files
Uses Envoy for enforcing the JWT auth: https://www.envoyproxy.io/docs/envoy/latest/api-v2/config/filter/http/jwt_authn/v2alpha/config.proto

Uses NatsProto for all Data transfer between client and server.
https://github.com/pion/ion/blob/master/pkg/proto/proto.go

https://github.com/pion/ion/blob/master/pkg/proto/biz.go

Make custom types is easy: EX: https://github.com/jbrady42/ion-vid/blob/master/types.go

## CHAT
Very clean chat layout: https://github.com/sopheamen007/app.mobile.facebook-messenger-app-ui
- perfect for adaptation i think.

# archi 03

No GRPC, and NO Envoy needed at edge at all.
Can run Consul to manage our nodes, services discovery, etc

SO Desktops can run a Server also because its only OpenAPI

So Clients can be cross compiled to WASM i think.

Everything is HTTP2, and so security is much easier.

SO can use Caddy :)
- Plugsin: https://caddy.community/t/list-of-caddy-2-modules/7839
- Git:
	- https://github.com/vrongmeal/caddygit
	- Great for anything we want to propgate outwards like config or websites
- TLS:
	- https://github.com/pteich/caddy-tlsconsul/

- Auth:

Liftbridge (LF)
- event store that pushes out to the edge

Dragonfly (DF)
- https://github.com/dragonflyoss/Dragonfly
- DOCS: https://github.com/dragonflyoss/Dragonfly/tree/master/docs
- Images store that pushes out to Edge, and all edges use P2P
- SO a new image is added and encrypted against the Users Public Key, and the event is added to LF
	- The image and event arrives at all edges ( does not matter because its all Crypted)
	- AuthZ and then pushes to all subscribers

vulcain (VC)
- https://github.com/dunglas/vulcain
- Does Http2-Push to push files to clients.
- SO allows use to do image Event Sourcing and sent the images to the client asnyc
- Also works perfectly with Service Worker. SO raises the distinct possibility of using GIO !!
- Converts Open API into PUSH
- caddy supports it: https://github.com/caddyserver/caddy/pull/3573#issuecomment-657206983

mercury (MC)
- https://github.com/dunglas/mercure
- Does SSE to push data to clients. Exactly the same as Vulcain but for data event sourcing.
- When Offline can send via: https://www.w3.org/TR/push-api/
- ! Its Critical to Check if Push-api and Notifications API working on Mobile !! 
	- Does not work on Mobiles
	- Add to Home screen (or A2HS for short), then does it work ?
		- Chrome YES
		- Windows Edge - NO
		- Safari - NO
		- Firefox - No

Outline (OL)
- https://github.com/outline/openapi
- Docker: https://github.com/chsasank/outline-wiki-docker-compose
- Defines the primary API that Client will use. Has what we need !
- Convert to Dart ?
# roadmap

Of what though ?

- Infra
	- dont use k8 or even docker. Cause no one can run it unless they are a senior dev basically.
	- so still with just binaries.
	- Need some global config like ETCD.

- Minimise networking and storage needed on servers to reduce cost and reliance.
	- implies P2P as much as possible
	- If two users NOT online then message will not get there until both online is not acceptable
		- So server needs to act as store and forward relay that holds them for and deleted them
		- Same goes for images.
		- SO NATS and LIftbridge running as baremetal over grpc.

- P2P basis
	- Implies using Ion so we have a basis for P2P over webrtc
	- currently Docker based due to the using redis. But thats ok.
	- Gives us a "find each other point"
	- Needs STUN and TURN servers also unless we reuse googles ?
	- PROB: WebRTC leaks your location !!!
		- see for yourself: https://browserleaks.com/webrtc
		- BUT is ONLY way to do video conf !!!

- Message Correlation
	- Prob: Any P2P that uses a store and forward pattern will break due to message ordering over time unless we have a CRDT and OT layer.
	- SO only way is to incorporate CRDT message correlation
	- Use IPFS: https://github.com/ipfs/go-ds-crdt
		- they use it for their server cluster currently. 
			- https://github.com/ipfs/ipfs-cluster
		- Is only designed for a WAL pattern which is Last WRITE Wins, and so only good for Servers

- Test the infra
	- really should write a Test harness that uses many clients and many servers to test this shit works
	
- With an architectural basis that works then we can make actual functionality.

- Global signup and user profile
	- Models: User ( is an Org itself), Orgs, Projects
	- Use email for lost password etc.


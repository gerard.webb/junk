# archi 07

Use envoy, GPPC so we have all that.
- Run with Consul to scale out. No K8 mess and block storage needed.
- They will later turn on QUIC when others do and we get it for free inside Envoy with zero change to our golang server codde
- Can run anywhere and have all the goodies of k8. Can also run in K8 if need be as Consul Envy supports K8.

## Envoy

Only does what we need it for so that we can do the other stuff in golang
- HTTPS / TLS
- GRPC and GRPC-web

AUth is done in golang. SO we manage the jwt and the flow so we can do custom stuff
Authz is done in golang


## HTTPS

2 options:
- manually install to disk
- Get Envoy to do it magically.

For lcoal dev us makecert: https://github.com/FiloSottile/mkcert

Manual: 
https://github.com/go-acme/lego
- used by: 
	- https://github.com/wondenge/otomatik

https://github.com/acmesh-official/acme.sh
- just bash script


## JWT, etc

Envoy can check the JWT for us
Has JWT: https://github.com/pion/ion/pull/303/files
Uses Envoy for enforcing the JWT auth: https://www.envoyproxy.io/docs/envoy/latest/api-v2/config/filter/http/jwt_authn/v2alpha/config.proto

Golang can model the Authz

https://github.com/goadesign/examples/tree/master/security

## GRPC and Flutter
 

https://dev.to/techschoolguru/use-grpc-interceptor-for-authorization-with-jwt-1c5h
https://gitlab.com/techschool/pcbook

With flutter

https://dev.to/carminezacc/user-authentication-jwt-authorization-with-flutter-and-node-176l
front end:https://github.com/carzacc/jwt-tutorial-flutter/blob/master/lib/main.dart
backend: https://github.com/carzacc/jwt-tutorial-backend-web

Jwt and grpc 

https://dev.to/techschoolguru/use-grpc-interceptor-for-authorization-with-jwt-1c5h
https://gitlab.com/techschool/pcbook

With flutter

https://dev.to/carminezacc/user-authentication-jwt-authorization-with-flutter-and-node-176l

## Other shit

It would still be nice to generate our own embed.
Then we woudl NOT need Envoy, because its all websockets or QUIC or SSE !
- DB can query local and backend at same time on a query and merge the result Isar is doing this !
- GRPC code for client is then golang, and then cross compiled to WASM and Native.
  - SO need to use websockets for WASM.


Make a embedded layer in golang.
- can do DB, networking, Auth all there.
- DB is Genji. Designed to work in WASM
- Auth. Easy and already done by others for RSA and PGP etc. Can do all over GRPC to Server too :)
- Networking
	- That means we need a golang layer that can run on Web as WASM that 100% conforms to GRPC
	- What does GRPC Web use ? Fetch or XHR, not web sockets. SO we need a GRPC-Web WASM client that uses that !
		- Microsoft has done it: https://blog.stevensanderson.com/2020/01/15/2020-01-15-grpc-web-in-blazor-webassembly/
	- Then we need the Flutter to Embed Bindings.
		- For web and Native its different. I VERY much recommend Protobufs here because WASM and Native have very complex tradeoffs and Protobufs works and its code generated again !
	- Candidates:
	- https://github.com/dennwc/dom/tree/master/net
		- Has webrtc and Websockets ! We need both eventually.
		- Forks: https://github.com/dennwc/dom/network

Server
- Pure GRPC
- GO cloud libs so we are portable between cloud and non cloud. Has PostreSQL, NATS and Minio there and the cloud equivalents.

Message Queue
- Holds messages to be relayed. For ever.
- Use LIftBridge for Origin to CDN Cache. https://github.com/liftbridge-io/liftbridge
- Nice GRPC so easy to work with Client using standard toolng.
- Auth needed.

Blob Queue
- Holds images, video, etc to be relayed. For ever.
- Use Dragon for for Origin to CDN edge caching. https://github.com/dragonflyoss/Dragonfly
- S3 API ?

Data
- Match the SQL up to the Protobufs
- Gen from the DB upwards, because DB will always be the source of truth, but Protobufs can change with Schema Evolution.
	- Gen from DB: https://github.com/kyleconroy/sqlc

